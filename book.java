package com.polarion.demo;

/**
* @wi.implements ELIBP-117 Returns book details including summary
*/
public class Book {

    private String title;
    private String author;
    private String publisher;
    private int publicationYear;
    private String summary;

    /**
     * Creates a new book.
     *
     * @param title
     * @param author
     * @param publisher
     * @param publicationYear
     */
    public Book(String title, String author, String publisher, int publicationYear, String summary) {
        this.title = title;
        this.author = author;
        this.publisher = publisher;
        this.publicationYear = publicationYear;
        this.summary = summary;

    }

    /**
     * Returns title of this book.
     */
    public String getTitle() {
        return this.title;
    }

    /**
     * Returns author of this book.
     */
    public String getAuthor() {
        return this.author;
    }

    /**
     * Returns publisher of this book.
     */
    public String getPublisher() {
        return this.publisher;
    }

    /**
     * Returns publication year of this book.
     */
    public int getPublicationYear() {
        return this.publicationYear;
    }

    /**
     * Returns summary of this book.
     */
    public String getSummary() {
        return this.summary;
    }

}
